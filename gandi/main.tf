data "gandi_domain" "domain" {
  name = var.domain_name
}

resource "gandi_livedns_record" "records" {
  for_each = { for i, v in var.dns_records : i => v }

  zone   = data.gandi_domain.domain.id
  name   = var.dns_records[each.key]["name"]
  type   = var.dns_records[each.key]["type"]
  ttl    = var.dns_records[each.key]["ttl"]
  values = var.dns_records[each.key]["values"]
}
