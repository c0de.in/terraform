variable "domain_name" {
  description = "Domain name"
  type        = string
  default     = "c0de.in"
}

variable "domain_ip" {
  description = "Domain IP"
  type        = string
  default     = "91.121.142.203"
}

variable "dns_records" {
  type = list(object({
    name   = string
    type   = string
    ttl    = number
    values = list(string)
  }))
  description = "DNS records to create in main zone"
  default = [{
    name   = ""
    type   = ""
    ttl    = 300
    values = ["91.121.142.203"]
  }]
}
