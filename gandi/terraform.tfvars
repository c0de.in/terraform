dns_records = [{
  name   = "4ll1n33di5"
  type   = "A"
  ttl    = 300
  values = ["91.121.142.203"]
  },
  {
    name   = "@"
    type   = "A"
    ttl    = 300
    values = ["91.121.142.203"]
  },
  {
    name   = "hosting"
    type   = "CNAME"
    ttl    = 300
    values = ["@"]
  },
  {
    name   = "vmm01"
    type   = "CNAME"
    ttl    = 300
    values = ["hosting"]
  },
  {
    name   = "web01"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.2"]
  },
  {
    name   = "nfs01"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.20"]
  },
  {
    name   = "prx01"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.21"]
  },
  {
    name   = "prx02"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.22"]
  },
  {
    name   = "api"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.30"]
  },
  {
    name   = "mks01"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.31"]
  },
  {
    name   = "nks01"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.41"]
  },
  {
    name   = "nks02"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.42"]
  },
  {
    name   = "nks03"
    type   = "A"
    ttl    = 300
    values = ["10.0.0.43"]
  },
  {
    name   = "ingress"
    type   = "A"
    ttl    = 300
    values = ["91.121.142.203"]
  },
  {
    name   = "*"
    type   = "CNAME"
    ttl    = 300
    values = ["ingress"]
  },
  {
    name   = "darons"
    type   = "A"
    ttl    = 300
    values = ["88.127.213.58"]
  },
  {
    name   = "freeland"
    type   = "A"
    ttl    = 300
    values = ["91.169.58.36"]
  },
  {
    name   = "vmm00"
    type   = "A"
    ttl    = 300
    values = ["149.202.80.54"]
  },
]
