terraform {
  required_providers {
    gandi = {
      version = "2.3.0"
      source  = "go-gandi/gandi"
    }
  }

  backend "s3" {
    bucket                      = "terraform"
    key                         = "gandi/terraform.tfstate"
    endpoint                    = "https://s3.c0de.in"
    region                      = "minio"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    force_path_style            = true
  }
}

provider "gandi" {}
