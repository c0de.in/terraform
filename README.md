# terraform

This repository contains Terraform code to manage Gandi DNS records & Proxmox VMs.
The state is managed in `s3.c0de.in`, the AWS environment variables need to be exported.

```bash
export AWS_ACCESS_KEY_ID=$(safe c0de.in/minio/u)
export AWS_SECRET_ACCESS_KEY=$(safe c0de.in/minio/p)
```

## gandi

```bash
export GANDI_KEY=$(safe gandi/api)
cd gandi && t apply
```

## pve

```bash
export PM_PASS=$(safe c0de.in/terraform/p)
cd pve && t apply
```
