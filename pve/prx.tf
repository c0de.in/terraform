data "template_file" "userdata_prx" {
  for_each = var.prx

  template = file("userdata.yml")

  vars = {
    hostname             = each.value.hostname
    domain               = var.domain
    ssh_pub_key          = file(var.ssh_pub_key)
    primary_dns_server   = var.primary_dns_server
    secondary_dns_server = var.secondary_dns_server
  }
}

resource "local_file" "pve_userdata_file_prx" {
  for_each        = var.prx
  content         = data.template_file.userdata_prx[each.key].rendered
  filename        = "userdata/${each.value.hostname}.yml"
  file_permission = "0644"
}

resource "null_resource" "cloud_init_config_files_prx" {
  for_each = var.prx

  connection {
    type     = "ssh"
    user     = var.pve_ssh_user
    password = var.pve_ssh_password
    host     = var.pve_ip
  }

  provisioner "file" {
    source      = local_file.pve_userdata_file_prx[each.key].filename
    destination = "/var/lib/vz/snippets/${each.value.hostname}.yml"
  }
}

resource "proxmox_vm_qemu" "prx" {
  depends_on = [
    null_resource.cloud_init_config_files_prx,
  ]


  for_each = var.prx

  target_node = var.pve_host["target_node"]
  vmid        = each.value.vmid
  name        = each.value.hostname
  clone       = "debian-11-tpl"
  os_type     = "cloud-init"
  qemu_os     = "l26"
  cicustom    = "user=local:snippets/${each.value.hostname}.yml"
  ipconfig0   = "ip=${each.value.ip}/24,gw=${cidrhost(format("%s/24", each.value.ip), 1)}"
  cores       = 1
  sockets     = 1
  vcpus       = 1
  memory      = 512
  bootdisk    = "virtio0"
  scsihw      = "virtio-scsi-pci"
  onboot      = true
  agent       = 1
  cpu         = "kvm64"
  numa        = false
  hotplug     = "network,disk,cpu"

  network {
    bridge = "vmbr0"
    model  = "virtio"
  }

  disk {
    type    = "scsi"
    storage = "local-hdd"
    size    = "20G"
  }
}

resource "null_resource" "ansible-prx" {
  depends_on = [
    proxmox_vm_qemu.prx,
  ]

  provisioner "local-exec" {
    working_dir = "../../ansible/"
    command     = "ansible-playbook -vbi hosts.yaml prx.yaml -l prx"
  }
}
