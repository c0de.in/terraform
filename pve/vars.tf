variable "pve_host" {
  type = map(any)
  default = {
    pm_api_url  = "https://vmm01.c0de.in:8006/api2/json"
    pm_user     = "terraform@pam"
    target_node = "vmm01"
  }
}

variable "pve_ip" {
  description = "The IP address of the proxmox server"
  type        = string
  default     = "vmm01.c0de.in"
}

variable "pve_ssh_user" {
  description = "The user who have access to the Proxmox API (with Administrator privilege)"
  type        = string
  default     = "terraform"
}

variable "pve_ssh_password" {
  description = "The password (overrided with $PM_PASS) to access to the Proxmox API (with Administrator privilege)"
  type        = string
  default     = ""
}

variable "ssh_pub_key" {
  description = "Your SSH public key"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}

variable "domain" {
  description = "VM domain"
  type        = string
  default     = "c0de.in"
}

variable "primary_dns_server" {
  description = "The primary DNS server of the VM"
  type        = string
  default     = "9.9.9.9"
}

variable "secondary_dns_server" {
  description = "The secondary DNS server of the VM"
  type        = string
  default     = "149.112.112.112"
}

variable "nfs_id" {
  description = "Starting ID for the NFS VM"
  type        = number
  default     = 200
}

variable "nfs_hostname" {
  description = "NFS hostname"
  type        = string
  default     = "nfs01"
}

variable "nfs_ip" {
  description = "IPs of the VMs, respective to the hostname order"
  type        = string
  default     = "10.0.0.20"
}

variable "prx" {
  type = map(any)
  default = {
    prx01 = {
      vmid     = 201
      hostname = "prx01"
      ip       = "10.0.0.21"
    },
    prx02 = {
      vmid     = 202
      hostname = "prx02"
      ip       = "10.0.0.22"
    }
  }
}

variable "k8s" {
  type = map(any)
  default = {
    mks01 = {
      vmid     = 301
      hostname = "mks01"
      ip       = "10.0.0.31"
    },
    nks01 = {
      vmid     = 401
      hostname = "nks01"
      ip       = "10.0.0.41"
    },
    nks02 = {
      vmid     = 402
      hostname = "nks02"
      ip       = "10.0.0.42"
    },
    nks03 = {
      vmid     = 403
      hostname = "nks03"
      ip       = "10.0.0.43"
    }
  }
}
