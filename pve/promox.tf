terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.11"
    }
  }

  backend "s3" {
    bucket                      = "terraform"
    key                         = "pve/terraform.tfstate"
    endpoint                    = "https://s3.c0de.in"
    region                      = "minio"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    force_path_style            = true
  }
}

provider "proxmox" {
  pm_api_url = var.pve_host["pm_api_url"]
  pm_user    = var.pve_host["pm_user"]
}
