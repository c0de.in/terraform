data "template_file" "userdata_nfs" {
  template = file("userdata.yml")

  vars = {
    hostname             = var.nfs_hostname
    domain               = var.domain
    ssh_pub_key          = file(var.ssh_pub_key)
    primary_dns_server   = var.primary_dns_server
    secondary_dns_server = var.secondary_dns_server
  }
}

resource "local_file" "pve_userdata_file_nfs" {
  content         = data.template_file.userdata_nfs.rendered
  filename        = "userdata/${var.nfs_hostname}.yml"
  file_permission = "0644"
}

resource "null_resource" "cloud_init_config_files_nfs" {
  connection {
    type     = "ssh"
    user     = var.pve_ssh_user
    password = var.pve_ssh_password
    host     = var.pve_ip
  }

  provisioner "file" {
    source      = local_file.pve_userdata_file_nfs.filename
    destination = "/var/lib/vz/snippets/${var.nfs_hostname}.yml"
  }
}

resource "proxmox_vm_qemu" "nfs" {
  depends_on = [
    null_resource.cloud_init_config_files_nfs,
  ]

  target_node = var.pve_host["target_node"]
  vmid        = var.nfs_id
  name        = var.nfs_hostname
  clone       = "debian-11-tpl"
  os_type     = "cloud-init"
  qemu_os     = "l26"
  cicustom    = "user=local:snippets/${var.nfs_hostname}.yml"
  ipconfig0   = "ip=${var.nfs_ip}/24,gw=${cidrhost(format("%s/24", var.nfs_ip), 1)}"
  cores       = 1
  sockets     = 1
  vcpus       = 1
  memory      = 1024
  bootdisk    = "virtio0"
  scsihw      = "virtio-scsi-pci"
  onboot      = true
  agent       = 1
  cpu         = "kvm64"
  numa        = false
  hotplug     = "network,disk,cpu"

  network {
    bridge = "vmbr0"
    model  = "virtio"
  }

  disk {
    type    = "scsi"
    storage = "local-hdd"
    size    = "20G"
  }

  disk {
    type    = "scsi"
    storage = "local-hdd"
    size    = "1500G"
  }
}

resource "null_resource" "ansible-nfs" {
  depends_on = [
    proxmox_vm_qemu.nfs,
  ]

  provisioner "local-exec" {
    working_dir = "../../ansible/"
    command     = "ansible-playbook -vbi hosts.yaml nfs.yaml -l nfs"
  }
}
